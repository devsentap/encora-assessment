import React, { Component } from 'react'

import 'bulma/css/bulma.min.css'
import {
  Section, Container, Content, Columns, Column,
  Field, Label, Control, Input, Button
} from 'bloomer'

class OccupancyPage extends Component {
  constructor(props) {
    super(props);

    this.updateInputValue = this.updateInputValue.bind(this);
    this.handleClick = this.handleClick.bind(this);

    this.state = {
      adults: 0,
      children: 0,
      infants: 0
    };
  }

  updateInputValue(evt) {
    switch (evt.target.id) {
      case 'adults': this.setState({ adults: parseInt(evt.target.value) }); break;
      case 'children': this.setState({ children: parseInt(evt.target.value) }); break;
      case 'infants': this.setState({ infants: parseInt(evt.target.value) }); break;
      default: break;
    }
  }

  handleClick() {
    const { adults, children, infants } = this.state;

    //Rules:
    //#1. Maximum guests 7 (excluding infants)
    //#2. No room of only children or infants (must have at least one adult)
    //#3. Max room of 3
    //#4. Booking rejected if guests > 7 or room > 3

    const MAX_GUEST = 7,
      MAX_ROOM = 3,
      MAX_ADULTS = 3,
      MAX_CHILDREN = 3,
      MAX_INFANTS = 3;

    let num_guest = adults + children;

    if (num_guest > MAX_GUEST) { document.getElementById('booking_status').textContent = "BOOKING REJECTED"; return; }

    let occupants = {
      adults: [],
      children: [],
      infants: []
    };

    let i = 0;
    for (; i < adults; i++) { occupants.adults.push(`A${i + 1}`); }
    i = 0;
    for (; i < children; i++) { occupants.children.push(`C${i + 1}`); }
    i = 0;
    for (; i < infants; i++) { occupants.infants.push(`I${i + 1}`); }

    let filled_rooms = [];

    fillRooms(occupants, filled_rooms);

    function getMaxNumFilledRoom(occupants) {
      let a = Math.ceil(occupants.adults.length / MAX_ADULTS),
        b = Math.ceil(occupants.children.length / MAX_CHILDREN),
        c = Math.ceil(occupants.infants.length / MAX_INFANTS);

      return Math.max(a, b, c);
    }

    function fillRooms(occupants, filled_rooms) {
      let num_rooms = getMaxNumFilledRoom(occupants);

      if (num_rooms > MAX_ROOM) { document.getElementById('booking_status').textContent = "BOOKING REJECTED"; return; }

      let j = 0;
      for (; j < num_rooms; j++) {
        filled_rooms.push({
          adults: [],
          children: [],
          infants: []
        });
      }

      //now that we have the max number of rooms, we can start to fill the rooms
      j = 0;
      while (occupants.adults.length > 0) {
        filled_rooms[j].adults = occupants.adults.splice(0, MAX_ADULTS);
        j++;
      }
      j = 0;
      while (occupants.children.length > 0) {
        filled_rooms[j].children = occupants.children.splice(0, MAX_CHILDREN);
        j++;
      }
      j = 0;
      while (occupants.infants.length > 0) {
        filled_rooms[j].infants = occupants.infants.splice(0, MAX_INFANTS);
        j++;
      }

      relocateAdults(filled_rooms);
      showRoomAssignment(filled_rooms);
    }

    function showRoomAssignment(filled_rooms) {
      let i = 0,
        html = "";

      for (; i < filled_rooms.length; i++) {
        html += `Room ${i + 1}: [` + filled_rooms[i].adults.join(',') + ',' + filled_rooms[i].children.join(',') + ',' + filled_rooms[i].infants.join(',') + ']';
        html += ' ';
      }

      document.getElementById('booking_status').textContent = "";
      document.getElementById('room_assignment').textContent = "";

      let booking_status = verifyAdultsPresence(filled_rooms);

      if (filled_rooms.length > 0) {
        document.getElementById('booking_status').textContent = booking_status ? "BOOKING ACCEPTED" : "BOOKING REJECTED";
        document.getElementById('room_assignment').textContent = html;
      }
    }

    //relocate available adults to rooms with children or infants
    function relocateAdults(filled_rooms) {
      let i = 0;

      for (; i < filled_rooms.length; i++) {
        if ((filled_rooms[i].children.length > 0 || filled_rooms[i].infants.length > 0) && filled_rooms[i].adults.length === 0) {
          let j = i;
          for (; j >= 0; j--) {
            if (filled_rooms[j].adults.length > 1) {
              filled_rooms[i].adults.push(filled_rooms[j].adults.pop());
            }
          }
        }
      }
    }

    //booking rejected if got children or infant, but no adult
    function verifyAdultsPresence(filled_rooms) {
      let i = 0;

      for (; i < filled_rooms.length; i++) {
        if ((filled_rooms[i].children.length > 0 || filled_rooms[i].infants.length > 0) && filled_rooms[i].adults.length === 0) {
          return false;
        }
      }

      return true;
    }
  }

  render() {
    return (
      <Section>
        <Container>
          <Columns isCentered>
            <Column isSize='1/4'>
              <Field>
                <Label>Adults: </Label>
                <Control>
                  <Input id="adults" type="number" min="0" value={this.state.adults} onChange={this.updateInputValue} />
                </Control>
              </Field>
            </Column>
            <Column isSize='1/4'>
              <Field>
                <Label>Children: </Label>
                <Control>
                  <Input id="children" type="number" min="0" value={this.state.children} onChange={this.updateInputValue} />
                </Control>
              </Field>
            </Column>
            <Column isSize='1/4'>
              <Field>
                <Label>Infants: </Label>
                <Control>
                  <Input id="infants" type="number" min="0" value={this.state.infants} onChange={this.updateInputValue} />
                </Control>
              </Field>
            </Column>
          </Columns>

          <Content hasTextAlign="centered">
            <Button isColor="success" isOutlined isSize="small" onClick={this.handleClick}>Book Room</Button>
          </Content>

          <Content hasTextAlign="centered">
            <p id="booking_status"></p>
            <p id="room_assignment"></p>
          </Content>
        </Container>
      </Section>
    );
  }
}

export default OccupancyPage